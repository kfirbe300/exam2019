// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCDn20DTKQOUV4Gestl2WcSsfzvRkBERZI",
    authDomain: "exam2019-7a14a.firebaseapp.com",
    databaseURL: "https://exam2019-7a14a.firebaseio.com",
    projectId: "exam2019-7a14a",
    storageBucket: "exam2019-7a14a.appspot.com",
    messagingSenderId: "321738368738"
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
